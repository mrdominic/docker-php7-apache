FROM php:7.2-apache-stretch

MAINTAINER dominic@thenewpress.net

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# timezone / date   
RUN echo "Europe/Berlin" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

# install system packages
RUN apt-get update -y && \
  apt-get install -y no-install-recommends \
  less vim nano wget unzip rsync git mysql-client autossh \
  libcurl4-openssl-dev libfreetype6 libjpeg62-turbo libpng-dev libjpeg-dev libxml2-dev libwebp6 libxpm4 libc-client-dev libkrb5-dev && \
  apt-get clean && \
  apt-get autoremove -y && \


# install php extensions
RUN docker-php-ext-install curl json xml mbstring zip bcmap soap pdo_mysql gd gettext imap


# apache

RUN /usr/sbin/a2enmod rewrite && /usr/sbin/a2enmod headers 
COPY ./files/000-default.conf /etc/apache2/sites-available/000-default.conf


WORKDIR /var/www/html

CMD ["apache2-foreground"]
