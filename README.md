# docker-php7-apache #

## ENV variables ##

`ServerAdmin ${PS_APACHE_SERVERADMIN}`

`DocumentRoot ${PS_APACHE_DOCROOT}`

`ErrorLog ${APACHE_LOG_DIR}/error.log`

`CustomLog ${APACHE_LOG_DIR}/access.log combined`